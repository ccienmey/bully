@extends('app')

@section('content')
<br><br><br>
    <div class="text">
        <h1 class="text-center" style="font-family: Secular One">About Bullying</h1>
        <h3 class="text-center" style="font-family: Lobster">
            Bullying adalah tindakan yang dilakukan seseorang atau sekelompok orang yang berusaha merendahkan, menyakiti,
            atau mengintimidasi orang lain secara terus-menerus dan dengan sengaja. Bullying dapat dilakukan secara fisik,
            verbal, atau melalui penggunaan media sosial atau teknologi. Bullying dapat terjadi di mana saja, baik di
            sekolah, tempat kerja, atau bahkan di lingkungan sosial. Dampak dari bullying bisa sangat berbahaya, termasuk
            menyebabkan masalah kesehatan mental seperti stres, depresi, dan kecemasan, serta dapat mempengaruhi kinerja
            akademik atau pekerjaan. Pencegahan bullying melibatkan upaya untuk meningkatkan kesadaran tentang masalah ini,
            serta memberikan dukungan dan sumber daya bagi mereka yang telah menjadi korban. Penting untuk mempromosikan
            budaya yang menghargai perbedaan dan memperlakukan semua orang dengan hormat dan martabat yang sama.
        </h3>
    </div>
    <br><br><br><br>
    <div class="col-sm-6 col-md-6">
        <img src="/img/he.png" style="width:100%">
    </div>
    <div class="col-sm-6 col-md-6">
        <h3 class="text-center" style="font-family: Lobster; font-size: 30px">
            Bullying adalah tindakan yang dilakukan secara berulang-ulang dan sengaja untuk menyakiti atau merendahkan orang lain secara fisik, verbal, atau psikologis. Berikut adalah beberapa ciri-ciri bullying:
        </h3>
        <li style="font-family: Lobster; font-size: 25px">
        1. Tindakan dilakukan secara berulang-ulang
        </li>
        <li style="font-family: Lobster; font-size: 25px">
        2. Sengaja dilakukan untuk menyakiti atau merendahkan orang lain
        </li>
        <li style="font-family: Lobster; font-size: 25px">
        3. Bentuk tindakan dapat berupa fisik, verbal, atau psikologis
        </li>
        <li style="font-family: Lobster; font-size: 25px">
        4. Target sering kali merasa takut, tidak aman, atau tertekan
        </li>
        <li style="font-family: Lobster; font-size: 25px">
        5. Tindakan dilakukan oleh seseorang atau sekelompok orang yang lebih kuat atau berkuasa dari target
        </li>
        <li style="font-family: Lobster; font-size: 25px">
        6. Tindakan bullying dapat terjadi di berbagai tempat, seperti di sekolah, tempat kerja, atau di lingkungan sosial
        </li>
        <li style="font-family: Lobster; font-size: 25px">
        7. Dapat memicu dampak jangka panjang pada target, seperti depresi, cemas, dan kecemasan sosial.
        </li>
    </div>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
@endsection

