@extends('app')

@section('content')
    <div class="container">
        <h1></h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Complaint_id</th>
                    <th>Response_date</th>
                    <th>Response</th>
                    <th>Officer_id</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                        <td>{{ $response->officer_id }}</td>
                        <td>
                            <a href="/admin/responses/{{ $response->id }}" class="btn btn-primary">Detail</a>
                            <form action="/admin/responses/{{ $response->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </form>
                            </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/responses/create" class="btn btn-success">Tambah</a>
    </div>
@endsection