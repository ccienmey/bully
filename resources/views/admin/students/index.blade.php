@extends('app')

@section('content')
    <div class="container">
        <h1></h1>
        <p>{{ $student_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>NISN</th>
                    <th>Class</th>
                    <th>User_id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $student->NISN }}</td>
                        <td>{{ $student->class }}</td>
                        <td>{{ $student->user->name }}</td>
                        <td>
                            <a href="/admin/students/{{ $student->id }}" class="btn btn-primary">Detail</a>
                            <form action="/admin/students/{{ $student->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </form>
                        </td>
                    </tr>
            @endforeach
            </tbody>
        </table>
        <a href="/admin/students/create" class="btn btn-success">Tambah</a>
    </div>
@endsection