<header class="border-bottom" id="navigation-bar" style="width: 151rem">
    <div class="container d-flex align-items-center d-print-none">
        <a href="/#" class="d-flex align-items-center me-3 text-decoration-none">
            <img src="/img/logo.png" style="font-size: 5px;"></i>
            <span class="fs-3 ms-2 d-print-none" id="aplikasi">ANPERA</span>
            <li><a href="/about" class="nav-link active fs-3 ms-2" style="font-size: 20px; margin-top: 10px"
                id="About">ABOUT BULLYING</a></li>
        </a>
        @if (auth()->user() && auth()->user()->level == 'admin')
            <ul class="nav me-auto d-print-none" style="margin-top: -10px">
                <li><a href="/admin/users" class="nav-link active" style="font-size: 17px" id="user">User</a></li>
                <li><a href="/admin/students" class="nav-link active" style="font-size: 17px;"
                        id="student">Student</a></li>
                <li><a href="/admin/complaints" class="nav-link active"style="font-size: 17px;"
                        id="complaint">Complaint</a></li>
                <li><a href="/admin/responses" class="nav-link active" style="font-size: 17px;"
                        id="response">Response</a></li>
                <li><a href="/admin/complaint-report" class="nav-link active"style="font-size: 17px;"
                        id="generate">Generate Complaint</a></li>
            </ul>
        @endif
        @if (auth()->user() == null)
            <ul class="nav ms-auto d-print-none">
                <li><a href="/login" class="nav-link active" id="login"><i class="bi bi-box-arrow-in-right"
                            style="font-size: 20px;"></i>
                        Login</a></li>
            </ul>
        @endif
        @if (auth()->user() != null)
            <div class="dropdown d-print-none ms-auto" id="user">
                <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                    <i class="bi bi-person-circle" style="font-size: 30px"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="/logout" class="dropdown-item">Log Out</a></li>
                </ul>
            </div>
        @endif
    </div>
    <div class="d-print-none" id="laporan">
        <h2 href="/form" class="text-center" style="color: white">LAPORAN PENGADUAN SEKOLAH GRACIA</h2>
    </div>
</header>
