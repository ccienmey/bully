@extends('app')

@section('content')
    <div class="container">
        <h1></h1>
        <p>{{ $student_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>NISN</th>
                    <th>Class</th>
                    <th>User_id</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($student_list as $student)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $student->NISN }}</td>
                        <td>{{ $student->class }}</td>
                        <td>{{ $student->user_id }}</td>
                        <td>
                            <a href="/officer/students/{{ $student->id }}" class="btn btn-primary">Detail</a>
                            <a href="#" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/officer/students/create" class="btn btn-success">Tambah</a>
    </div>
@endsection