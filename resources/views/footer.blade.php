v<!-- Site footer -->
  <br><br>
  <footer class="site-footer d-print-none" style=" margin-right: 0px; padding: 65px; width: 151rem" id="pilihan">
      <div class="container">
          <div class="footer2">
              <div class="row">
                  <div class="col-sm-12 col-md-6">
                      <h6>SMK GRACIA BANDUNG</h6>
                      <i class="fa fa-map-marker"></i>
                      <span>Jl. Cibadak Gg. Sereh, no. 26 Kec. Astanaanyar Kota Bandung, Jawa Barat</span>
                      <br>
                      <i class="fa fa-phone"></i>
                      <span>022-6014810</span>
                  </div>
                  <div class="col-xs-3 col-md-3">
                      <h6>ABOUT SMK GRACIA</h6>
                      <span>Sekolah Gracia adalah sekolah umum yang memiliki keanekaragaman dalam memberikan pendidikan
                          agama sehingga terdapat Agama Islam, Kristen Protestan, Kristen Khatolik dan Budha dari
                          tingkat TK sampai dengan tingkat SMA</span>
                  </div>
                  <br><br><br><br>
                  <div class="col-xs-3 col-md-3">
                      <h6>Sosial Account</h6>
                      <div class="footer-icons" style="margin-bottom: 10px margin-right: 65px display-flex">
                          <a href="https://instagram.com/gracia_bandung?igshid=ODM2MWFjZDg"><i class="bi bi-instagram" id="ig"></i></a>
                          <a href="https://www.tiktok.com/@smpgraciabandung?_t=8akVrpCUnsA&_r=1"><i class="bi bi-tiktok" id="tt"></i></a>
                          <a href="https://youtube.com/@smkgraciabandung7017"><i class="bi bi-youtube" id="yt"></i></a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </div>
      </div>
  </footer>