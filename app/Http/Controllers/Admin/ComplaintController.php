<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Complaint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ComplaintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complaints = Complaint::paginate(5);
        return view('admin.complaints.index', ['complaint_list' => $complaints]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.complaints.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'complaint_date' => 'required',
            'user_id' => 'required',
            'content_report' => 'required',
            'photo' => 'required|file',
            'status' => 'required',
        ]);

        $path = $request->photo->store('public/images');
        $data['photo'] = str_replace('public/', '', $path);     

        Complaint::create($data);

        return redirect('/admin/complaints');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $complaint = Complaint::find($id);
        return view('admin.complaints.detail', ['complaint' => $complaint]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'complaint_date' => 'required',
            'user_id' => 'required',
            'content_report' => 'required',
            'status' => 'required',
        ]);

           complaint::where('id', $id)->update($data);

           return redirect('/admin/complaints');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Complaint::destroy($id);
            return redirect('/admin/complaints');
        } catch (QueryException $exc) {
            return redirect('/admin/complaints')
                ->withErrors([
                    'msg' => 'Complaint ' . $id . ' cannot be deleted because related with other entity'
                ]);
        }
    }
}
    