<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function show()
    {
        return view('login');
    }
    public function check(Request $request)
    {
        $data = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
        Auth::logout();
        $request->session()->invalidate();
        if (Auth::attempt($data)) {
            $request->session()->regenerate();

            $user =  Auth::user();
            if ($user->level == 'admin') {
                return redirect('/admin');
            } else if ($user->level == 'operator') {
                return redirect('/operator');
            }  else if ($user->level == 'student') {
                return redirect('/student');
            }
        }

        return back()->withErrors([
            'msg' => 'Username or Password invalid',
        ]);
    }
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}