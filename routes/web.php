<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\Officer\UserController as OfficerUserController;
use App\Http\Controllers\Admin\StudentController as AdminStudentController;
use App\Http\Controllers\Admin\ResponseController as AdminResponseController;
use App\Http\Controllers\Admin\ComplaintController as AdminComplaintController;
use App\Http\Controllers\Officer\StudentController as OfficerStudentController;
use App\Http\Controllers\Officer\ResponseController as OfficerResponseController;
use App\Http\Controllers\Student\ResponseController as StudentResponseController;
use App\Http\Controllers\Officer\ComplaintController as OfficerComplaintController;
use App\Http\Controllers\Student\ComplaintController as StudentComplaintController;
use Illuminate\Auth\Events\Logout;
use App\Http\Controllers\Admin\SummaryController as AdminSummaryController;
use App\Http\Controllers\Officer\SummaryController as OfficerSummaryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('aplikasi');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/admin', function () {
        return view('aplikasi');
    });
    Route::resource('/admin/users', AdminUserController::class);
    Route::resource('/admin/students', AdminStudentController::class);
    Route::resource('/admin/complaints', AdminComplaintController::class);
    Route::resource('/admin/responses', AdminResponseController::class);

    Route::get('/officer', function () {
        return view('aplikasi');
    });
    Route::resource('/officer/users', OfficerUserController::class);
    Route::resource('/officer/students', OfficerStudentController::class);
    Route::resource('/officer/complaints', OfficerComplaintController::class);
    Route::resource('/officer/responses', OfficerResponseController::class);

    Route::get('/student', function () {
        return view('aplikasi');
    });
    Route::resource('/student/responses', StudentResponseController::class);
    Route::resource('/student/complaints', StudentComplaintController::class);
});
    
    
    
Route::get('/login', [LoginController::class, 'show'])->name('login');
Route::post('/login', [LoginController::class, 'check']);
Route::get('/logout', [LoginController::class, 'logout']);
Route::get('/admin/complaint-report', [AdminSummaryController::class, 'complaintReport']);
Route::get('/officer/complaint-report', [AdminSummaryController::class, 'complaintReport']);

    
Route::get('/about', function () {
    return view('about');
});

Route::get('/laporkan', function () {
    return view('laporkan');
});

Route::get('/aplikasi', function () {
    return view('aplikasi');
});

Route::get('/logout', [LoginController::class, 'logout']);

