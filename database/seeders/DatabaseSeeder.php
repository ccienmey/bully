<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Complaint;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    
    public function run()
    {
        {
        $user_1 = User::create([
            'name' => 'Jono',
            'gender' => 'Laki-Laki',
            'date_of_birth' => '1999-10-05',
            'username' => 'Jono123',
            'password' => Hash::make('Coba_123'),
            'email' => 'jono123@gmail.com',
            'phone' => '08586004455',
            'address' => 'Jalan Kutilang no.100',
            'level' => 'student',
        ]);
        
        $user_2 = User::create([
            'name' => 'Toni',
            'gender' => 'Laki-Laki',
            'date_of_birth' => '2000-05-30',
            'username' => 'Toni56',
            'password' => Hash::make('Coba_123'),
            'email' => 'toni56@gmail.com',
            'phone' => '08583004455',
            'address' => 'Jalan Camar no.80',
            'level' => 'student',
        ]);

        $user_3 = User::create([
            'name' => 'Pujiani',
            'gender' => 'Perempuan',
            'date_of_birth' => '1998-11-15',
            'username' => 'Pujiani78',
            'password' => Hash::make('Coba_123'),
            'email' => 'pujiani78@gmail.com',
            'phone' => '085834564455',
            'address' => 'Jalan Bengawan no.14',
            'level' => 'officer',
        ]);

        $user_4 = User::create([
            'name' => 'Tini',
            'gender' => 'Perempuan',
            'date_of_birth' => '1998-11-15',
            'username' => 'Tini78',
            'password' => Hash::make('Coba_123'),
            'email' => 'tinii78@gmail.com',
            'phone' => '085834564455',
            'address' => 'Jalan Aceh no.14',
            'level' => 'admin',
        ]);

        $user_5 = User::create([
            'name' => 'Edoh',
            'gender' => 'Perempuan',
            'date_of_birth' => '1999-11-15',
            'username' => 'Edoh78',
            'password' => Hash::make('Coba_123'),
            'email' => 'edoh78@gmail.com',
            'phone' => '085832564455',
            'address' => 'Jalan Ketek no.15',
            'level' => 'officer',
        ]);
        
        $student_1 = Student::create([
            'NISN' => '0053090047',
            'class' => '12',
            'user_id' => $user_1->id,
        ]);

        $student_2 = Student::create([
            'NISN' => '005355540',
            'class' => '11',
            'user_id' => $user_2->id,
        ]);

        $student_3 = Student::create([
            'NISN' => '005360007',
            'class' => '12',
            'user_id' => $user_3->id,
        ]);

        $student_4 = Student::create([
            'NISN' => '0053070708',
            'class' => '12',
            'user_id' => $user_4->id,
        ]);

        $student_5 = Student::create([
            'NISN' => '0053090007',
            'class' => '11',
            'user_id' => $user_5->id,
        ]);

        $complaint_1 = Complaint::create([
            'complaint_date' => '2023-03-30',
            'content_report' => 'saat saya sedang keluar dari kamar mandi, tiba tiba tas saya disembunyikanhhh',
            'photo' => 'jjj',
            'status' => 'verifed',
            'user_id' => $user_1->id,
        ]);

        $complaint_2 = Complaint::create([
            'complaint_date' => '2023-05-30',
            'content_report' => 'saat saya sedang keluar dari kamar mandi, tiba tiba kepala saya dipukul',
            'photo' => 'jjj',
            'status' => 'new',
            'user_id' => $user_2->id,
        ]);

        $complaint_3 = Complaint::create([
            'complaint_date' => '2023-02-15',
            'content_report' => 'saat saya sedang keluar dari kelas, tiba tiba jaket saya dibakar oleh kaka kelas',
            'photo' => 'jjj',
            'status' => 'verifed',
            'user_id' => $user_3->id,
        ]);

        $complaint_4 = Complaint::create([
            'complaint_date' => '2023-06-17',
            'content_report' => 'saat saya sedang keluar dari kamar mandi, tiba tiba saya ditonjok oleh ketua osis',
            'photo' => 'jjj',
            'status' => 'done',
            'user_id' => $user_4->id,
        ]);

        $complaint_5 = Complaint::create([
            'complaint_date' => '2023-04-30',
            'content_report' => 'saat saya sedang keluar dari kamar mandi, tiba tiba saya disentil oleh Amel',
            'photo' => 'jjj',
            'status' => 'reject',
            'user_id' => $user_1->id,
        ]);

        $response = Response::create([
            'complaint_id' => $complaint_1->id,
            'response_date' => '2023-04-01',
            'response' => 'Ditindak Lanjuti',
            'officer_id' =>  $user_1->id,
        ]);

        $response = Response::create([
            'complaint_id' => $complaint_2->id,
            'response_date' => '2023-06-05',
            'response' => 'Belum Ditindak Lanjuti',
            'officer_id' =>  $user_2->id,
        ]);

        $response = Response::create([
            'complaint_id' => $complaint_3->id,
            'response_date' => '2023-02-19',
            'response' => 'Ditindak Lanjuti Dengan Jalur Hukum',
            'officer_id' =>  $user_3->id,
        ]);

        $response = Response::create([
            'complaint_id' => $complaint_4->id,
            'response_date' => '2023-06-19',
            'response' => 'Sedang Di Proses',
            'officer_id' =>  $user_4->id,
        ]);

        $response = Response::create([
            'complaint_id' => $complaint_5->id,
            'response_date' => '2023-05-03',
            'response' => 'Ditindak Lanjuti Secara Kekeluargaan',
            'officer_id' =>  $user_5->id,
        ]);
    
    }
}
}
